import { ApolloClient, ApolloLink, InMemoryCache } from "@apollo/client";
import { RestLink } from "apollo-link-rest";
import { HttpLink } from "apollo-link-http";
// const httpLink = new HttpLink({
//   uri: "http://localhost:8881"
// });

const restLink = new RestLink({
    endpoints: {
      contactapi: "http://127.0.0.1:8881",
    },
  });
  



  export const client = new ApolloClient({
    cache: new InMemoryCache(),
    link: ApolloLink.from([restLink]),
    // link: ApolloLink.from([restLink, httpLink]),
  });