import React from "react";
import { useQuery, gql } from "@apollo/client";

const STAFF_CONTACTS = gql`
query GetContacts {
    contactList @rest(type: "contactList", path: "/contacts", endpoint: "contactapi"){
    contacts{ 
        phone
        contactname
        office
        id
        dept
    }
    } 
  }
`;


function PhoneDirectory() {
  const { data, loading, error } = useQuery(STAFF_CONTACTS);

  if (loading) {
    return <div>loading...</div>;
  }

  if (error) {
    return <div>{error}</div>;
  }


    return  data.contactList.contacts.map(({id, contactname, phone }) => (
   <div key={id}>
      <p>
      {contactname}: {phone}
      </p>
    </div>
  ));
  

}

export default PhoneDirectory;