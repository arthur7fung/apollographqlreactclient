import logo from './logo.svg';
import './App.css';
import { client } from "./ApolloClient/client";
import { ApolloProvider } from '@apollo/client';
import PhoneDirectory from './PhoneDirectory';

function App() {
  return (
    <ApolloProvider client={client}>
      <div className="App">
        <PhoneDirectory />
      </div>
    </ApolloProvider>
  );
}

export default App;
