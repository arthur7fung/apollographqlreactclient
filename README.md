# Getting Started with 

Please run the Tornado server before running this FrontEnd Client

# apolloClient
Apollo GraphQL client simple React web app example 

# project package installation
yarn add @apollo/client graphql
yarn add graphql-anywhere apollo-link-rest
yarn add graphql-anywhere apollo-link-http
npm install

# Start server on default port 3000
npm run start

